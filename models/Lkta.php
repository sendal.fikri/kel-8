<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lkta".
 *
 * @property int $id
 * @property string $nama
 * @property string $nim
 * @property int $idstudi
 * @property string $masastudi
 * @property int $idpembimbing
 * @property string $judulta
 * @property string $tglpengta
 * @property string $status
 * @property string $tglsp
 * @property string $semsp
 * @property string $statussp
 * @property string $waktupengsp
 * @property int $idruangansp
 * @property int $idpengujisp
 * @property string $nilaipemsp
 * @property string $nilaipengsp
 * @property string $rataratasp
 * @property string $semsh
 * @property string $tglmulaish
 * @property string $tglakhirsh
 * @property string $tglpengsh
 * @property string $persyaratansh
 * @property string $statussh
 * @property int $idruangansh
 * @property int $idpengujish
 * @property string $nilaipemsh
 * @property string $nilaipengsh
 * @property string $rataratash
 * @property string $tglsidangsta
 * @property string $persidangsta
 * @property string $persyaratanuangsta
 * @property string $statussta
 * @property string $waktusta
 * @property int $idruangansta
 * @property int $idpengujusta
 * @property int $idpengujustaa
 * @property string $nilaipemsta
 * @property string $nilaipengsta
 * @property string $nilaipengstaa
 * @property string $ratasta
 * @property string $total
 * @property string $abjad
 * @property string $ipk
 */
class Lkta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lkta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'nim', 'idstudi', 'masastudi', 'idpembimbing', 'judulta', 'tglpengta', 'status'], 'required'],
            [['idstudi', 'idpembimbing', 'idruangansp', 'idpengujisp', 'idruangansh', 'idpengujish', 'idruangansta', 'idpengujusta', 'idpengujustaa'], 'integer'],
            [['masastudi', 'tglpengta', 'tglsp', 'waktupengsp', 'tglmulaish', 'tglakhirsh', 'tglpengsh', 'tglsidangsta', 'waktusta'], 'safe'],
            [['judulta', 'semsp', 'semsh', 'persyaratansh', 'persidangsta', 'persyaratanuangsta'], 'string'],
            [['nama'], 'string', 'max' => 100],
            [['nim', 'status', 'statussp', 'nilaipemsp', 'nilaipengsp', 'rataratasp', 'statussh', 'nilaipemsh', 'nilaipengsh', 'rataratash', 'statussta', 'nilaipemsta', 'nilaipengsta', 'nilaipengstaa', 'ratasta', 'total', 'abjad', 'ipk'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama Mahasiswa',
            'nim' => 'Nim',
            'idstudi' => 'Program Studi',
            'masastudi' => 'Angkatan',
            'idpembimbing' => 'Pembimbing',
            'judulta' => 'Judul Tugas Akhir',
            'tglpengta' => 'Tanggal Pengajuan Tugas Akhir',
            'status' => 'Status',
            'tglsp' => 'Tanggal Pengajuan Seminar Proposal',
            'semsp' => 'Sem.',
            'statussp' => 'Status',
            'waktupengsp' => 'Waktu',
            'idruangansp' => 'Ruangan',
            'idpengujisp' => 'Penguji',
            'nilaipemsp' => 'Pembimbing (Nilai)',
            'nilaipengsp' => 'Penguji (Nilai)',
            'rataratasp' => 'Rata-rata',
            'semsh' => 'Sem.',
            'tglmulaish' => 'Tanggal Mulai Seminar Hasil',
            'tglakhirsh' => 'Tanggal Akhir Seminar Hasil',
            'tglpengsh' => 'Tanggal Pengajuan Seminar Hasil',
            'persyaratansh' => 'Persyaratan',
            'statussh' => 'Status',
            'idruangansh' => 'Ruangan',
            'idpengujish' => 'Penguji',
            'nilaipemsh' => 'Pembimbing (Nilai)',
            'nilaipengsh' => 'Penguji (Nilai)',
            'rataratash' => 'Rata-rata (Nilai)',
            'tglsidangsta' => 'Tanggal Sidang Tugas Akhir',
            'persidangsta' => 'Persyaratan Sidang (Bimbingan)',
            'persyaratanuangsta' => 'Persyaratan Sidang (Bebas Keuangan)',
            'statussta' => 'Status',
            'waktusta' => 'Waktu',
            'idruangansta' => 'Ruangan',
            'idpengujusta' => 'Penguji (1)',
            'idpengujustaa' => 'Penguji (2)',
            'nilaipemsta' => 'Pembimbing (Nilai)',
            'nilaipengsta' => 'Penguji (Nilai 1)',
            'nilaipengstaa' => 'Penguji (Nilai 2)',
            'ratasta' => 'Rata-rata',
            'total' => 'Total',
            'abjad' => 'Abjad',
            'ipk' => 'Ipk',
        ];
    }
}
