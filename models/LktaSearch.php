<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lkta;

/**
 * LktaSearch represents the model behind the search form of `app\models\Lkta`.
 */
class LktaSearch extends Lkta
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'idstudi', 'idpembimbing', 'idruangansp', 'idpengujisp', 'idruangansh', 'idpengujish', 'idruangansta', 'idpengujusta', 'idpengujustaa'], 'integer'],
            [['nama', 'nim', 'masastudi', 'judulta', 'tglpengta', 'status', 'tglsp', 'semsp', 'statussp', 'waktupengsp', 'nilaipemsp', 'nilaipengsp', 'rataratasp', 'semsh', 'tglmulaish', 'tglakhirsh', 'tglpengsh', 'persyaratansh', 'statussh', 'nilaipemsh', 'nilaipengsh', 'rataratash', 'tglsidangsta', 'persidangsta', 'persyaratanuangsta', 'statussta', 'waktusta', 'nilaipemsta', 'nilaipengsta', 'nilaipengstaa', 'ratasta', 'total', 'abjad', 'ipk'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lkta::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'idstudi' => $this->idstudi,
            'masastudi' => $this->masastudi,
            'idpembimbing' => $this->idpembimbing,
            'tglpengta' => $this->tglpengta,
            'tglsp' => $this->tglsp,
            'waktupengsp' => $this->waktupengsp,
            'idruangansp' => $this->idruangansp,
            'idpengujisp' => $this->idpengujisp,
            'tglmulaish' => $this->tglmulaish,
            'tglakhirsh' => $this->tglakhirsh,
            'tglpengsh' => $this->tglpengsh,
            'idruangansh' => $this->idruangansh,
            'idpengujish' => $this->idpengujish,
            'tglsidangsta' => $this->tglsidangsta,
            'waktusta' => $this->waktusta,
            'idruangansta' => $this->idruangansta,
            'idpengujusta' => $this->idpengujusta,
            'idpengujustaa' => $this->idpengujustaa,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'nim', $this->nim])
            ->andFilterWhere(['like', 'judulta', $this->judulta])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'semsp', $this->semsp])
            ->andFilterWhere(['like', 'statussp', $this->statussp])
            ->andFilterWhere(['like', 'nilaipemsp', $this->nilaipemsp])
            ->andFilterWhere(['like', 'nilaipengsp', $this->nilaipengsp])
            ->andFilterWhere(['like', 'rataratasp', $this->rataratasp])
            ->andFilterWhere(['like', 'semsh', $this->semsh])
            ->andFilterWhere(['like', 'persyaratansh', $this->persyaratansh])
            ->andFilterWhere(['like', 'statussh', $this->statussh])
            ->andFilterWhere(['like', 'nilaipemsh', $this->nilaipemsh])
            ->andFilterWhere(['like', 'nilaipengsh', $this->nilaipengsh])
            ->andFilterWhere(['like', 'rataratash', $this->rataratash])
            ->andFilterWhere(['like', 'persidangsta', $this->persidangsta])
            ->andFilterWhere(['like', 'persyaratanuangsta', $this->persyaratanuangsta])
            ->andFilterWhere(['like', 'statussta', $this->statussta])
            ->andFilterWhere(['like', 'nilaipemsta', $this->nilaipemsta])
            ->andFilterWhere(['like', 'nilaipengsta', $this->nilaipengsta])
            ->andFilterWhere(['like', 'nilaipengstaa', $this->nilaipengstaa])
            ->andFilterWhere(['like', 'ratasta', $this->ratasta])
            ->andFilterWhere(['like', 'total', $this->total])
            ->andFilterWhere(['like', 'abjad', $this->abjad])
            ->andFilterWhere(['like', 'ipk', $this->ipk]);

        return $dataProvider;
    }
}
