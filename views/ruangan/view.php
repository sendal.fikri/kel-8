<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ruangan */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Ruangan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ruangan-view">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content" style="padding-left:0; padding-right:0">
                <div class="box box-info">
                    <section class="content">
                        <p>
                            <?= Html::a('Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </p>

                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                // 'id',
                                'nama',
                            ],
                        ]) ?>
                    </section>
                </div>
            </section>
        </div>
    </div>

</div>