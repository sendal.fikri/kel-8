<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Penguji */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penguji-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content" style="padding-left:0; padding-right:0">
                <div class="box box-info">
                    <section class="content">
                        <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
                        </div>
                    </section>
                </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>