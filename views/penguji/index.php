<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PengujiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penguji';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penguji-index">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content" style="padding-left:0; padding-right:0">
                <div class="box box-info">
                    <section class="content">
                        <p>
                            <?= Html::a('Input Penguji', ['create'], ['class' => 'btn btn-success']) ?>
                        </p>

                        <?php // echo $this->render('_search', ['model' => $searchModel]); 
                        ?>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                // 'id',
                                'nama',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </section>
                </div>
        </div>
    </div>
</div>