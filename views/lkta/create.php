<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lkta */

$this->title = 'Input';
$this->params['breadcrumbs'][] = ['label' => 'LKTA', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lkta-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>