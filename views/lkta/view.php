<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Lkta */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'LKTA', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="lkta-view">
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content" style="padding-left:0; padding-right:0">
                <div class="box box-info">
                    <section class="content">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                // 'id',
                                'nama',
                                'nim',
                                'idstudi',
                                'masastudi',
                                'idpembimbing',
                                'judulta:ntext',
                                'tglpengta',
                                'status',
                                'tglsp',
                                'semsp:ntext',
                                'statussp',
                                'waktupengsp',
                                'idruangansp',
                                'idpengujisp',
                                'nilaipemsp',
                                'nilaipengsp',
                                'rataratasp',
                                'semsh:ntext',
                                'tglmulaish',
                                'tglakhirsh',
                                'tglpengsh',
                                'persyaratansh:ntext',
                                'statussh',
                                'idruangansh',
                                'idpengujish',
                                'nilaipemsh',
                                'nilaipengsh',
                                'rataratash',
                                'tglsidangsta',
                                'persidangsta:ntext',
                                'persyaratanuangsta:ntext',
                                'statussta',
                                'waktusta',
                                'idruangansta',
                                'idpengujusta',
                                'idpengujustaa',
                                'nilaipemsta',
                                'nilaipengsta',
                                'nilaipengstaa',
                                'ratasta',
                                'total',
                                'abjad',
                                'ipk',
                            ],
                        ]) ?>
                    </section>
                </div>
            </section>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content" style="padding-left:0; padding-right:0">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-10">
                                <legend><i class="fa fa-book"></i> &nbsp; Lembar Kendali Tugas Akhir</legend>
                            </div>
                            <div class="col-md-2">

                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-6">
                                <dl class="dl-horizontal">
                                    <dt>Mahasiswa</dt>
                                    <dd>Muhammad Ardiansyah</dd>
                                    <dt>NIM</dt>
                                    <dd>0110218049</dd>
                                    <dt>Program Studi</dt>
                                    <dd>Informatika</dd>
                                    <dt>Masa Studi</dt>
                                    <dd>2018 s/d 2022</dd>
                                </dl>
                            </div>
                            <div class="col-md-5">
                                <dl class="dl-horizontal">
                                    <dt>Pembimbing</dt>
                                    <dd>Tubagus Rizky Dharmawan, S.T., M.Sc.</dd>
                                    <dt>Judul Tugas Akhir</dt>
                                    <dd><span class="text-aqua"><b>Lembar Kendali Tugas Akhir</b></span></dd>
                                    <dt>Tanggal Pengajuan Topik TA</dt>
                                    <dd>20 Mei 2020</dd>
                                    <dt>Status Mahasiswa</dt>
                                    <dd>Aktif</dd>
                                </dl>
                            </div>
                        </div>
                        <div class="detail-invoice">
                            <legend><i class="fa fa-book"></i> &nbsp;Seminar Proposal</legend>
                            <!-- <div id="w0" class="grid-view table-responsive"> -->
                            <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-6">
                                    <dl class="dl-horizontal">
                                        <dt>Tanggal Pengajuan Sem. Proposal</dt>
                                        <dd>15 Oktober 2020</dd>
                                        <dt>Sem</dt>
                                        <dd>-</dd>
                                        <dt>Status</dt>
                                        <dd>Sudah</dd>
                                        <dt>Jam-Hari-Tanggal</dt>
                                        <dd>08.30 WIB - Jumat, 18 Oktober 2019</dd>
                                        <dt>Ruangan</dt>
                                        <dd>B1-204</dd>
                                    </dl>
                                </div>
                                <div class="col-md-5">
                                    <dl class="dl-horizontal">
                                        <dt>Penguji</dt>
                                        <dd>Tubagus Rizky Dharmawan, S.T., M.Sc.</dd>
                                        <dt>Pembimbing (Nilai)</dt>
                                        <dd><span class="text-aqua"><b>90</b></span></dd>
                                        <dt>Penguji (Nilai)</dt>
                                        <dd>90</dd>
                                        <dt>Rata-rata (Nilai)</dt>
                                        <dd>90</dd>
                                    </dl>
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                        <div class="history-pembayaran">
                            <legend><i class="fa fa-book"></i> &nbsp;Seminar Hasil</legend>
                            <!-- <div id="w1" class="grid-view table-responsive"> -->
                            <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-6">
                                    <dl class="dl-horizontal">
                                        <dt>Tanggal Pengajuan Sidang TA</dt>
                                        <dd>-</dd>
                                        <dt>Persyaratan Sidang Bimbingan</dt>
                                        <dd>-</dd>
                                        <dt>Persyaratan Sidang Bebas Uang</dt>
                                        <dd>-</dd>
                                        <dt>Status</dt>
                                        <dd>Sudah</dd>
                                        <dt>Waktu</dt>
                                        <dd>00.00 day 00-00-0000</dd>
                                        <dt>Ruangan</dt>
                                        <dd>-</dd>
                                    </dl>
                                </div>
                                <div class="col-md-5">
                                    <dl class="dl-horizontal">
                                        <dt>Ruangan</dt>
                                        <dd>-</dd>
                                        <dt>Penguji</dt>
                                        <dd><span class="text-aqua"><b>-</b></span></dd>
                                        <dt>Pembimbing (Nilai)</dt>
                                        <dd>-</dd>
                                        <dt>Penguji (Nilai)</dt>
                                        <dd>-</dd>
                                        <dt>Rata-rata (Nilai)</dt>
                                        <dd>-</dd>
                                    </dl>
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                        <div class="history-pembayaran">
                            <legend><i class="fa fa-book"></i> &nbsp;Sidang Tugas Akhir</legend>
                            <!-- <div id="w1" class="grid-view table-responsive"> -->
                            <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-6">
                                    <dl class="dl-horizontal">
                                        <dt>Tanggal Pengajuan Sidang TA</dt>
                                        <dd>-</dd>
                                        <dt>Persyaratan Sidang Bimbingan</dt>
                                        <dd>-</dd>
                                        <dt>Persyaratan Sidang Bebas Keuangan</dt>
                                        <dd>-</dd>
                                        <dt>Status</dt>
                                        <dd>-</dd>
                                        <dt>Waktu</dt>
                                        <dd>00.00 day 00-00-0000</dd>
                                        <dt>Ruangan</dt>
                                        <dd>-</dd>
                                    </dl>
                                </div>
                                <div class="col-md-5">
                                    <dl class="dl-horizontal">
                                        <dt>Penguji 1</dt>
                                        <dd>-</dd>
                                        <dt>Penguji 2</dt>
                                        <dd><span class="text-aqua"><b>-</b></span></dd>
                                        <dt>Pembimbing (Nilai)</dt>
                                        <dd>-</dd>
                                        <dt>Penguji 1 (Nilai)</dt>
                                        <dd>-</dd>
                                        <dt>Rata-rata (Nilai)</dt>
                                        <dd>-</dd>
                                    </dl>
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                        <div class="history-pembayaran">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-6">
                                    <dl class="dl-horizontal">
                                        <p class="lead"><b>Keterangan:</b></p>
                                        <ul style="font-style:italic;">
                                            <li><span class="label label-default">BELUM TERSEDIA</span> : Dosen yang bersangkutan belum
                                                mem-publish nilai.</li>
                                            <li><span class="label label-warning">BELUM ISI KUISIONER</span> : Isilah kuisioner pelaksanaan
                                                perkuliahan terlebih dahulu untuk melihat nilai.</li>
                                            <li><span class="label label-success">Sudah diisi</span> : Anda sudah mengisi kuisioner
                                                pelaksanaan perkuliahan.</li>
                                        </ul>
                                    </dl>
                                </div>
                                <div class="col-md-5">
                                    <dl class="dl-horizontal">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th style="width:50%">Nilai Tugas Akhir</th>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Total</th>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Abjad</th>
                                                        <td>-</td>
                                                    </tr>
                                                    <tr>
                                                        <th>IPK LULUS</th>
                                                        <td>-</td>
                                                    </tr>
                                                    <br>
                                                    <tr>
                                                        <td>
                                                            <?= Html::a('Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-block btn-success']) ?>
                                                        </td>
                                                        <td>
                                                            <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
                                                                'class' => 'btn btn-block btn-danger',
                                                                'data' => [
                                                                    'confirm' => 'Apakah Anda Yakin Ingin Menghapus Data Ini?',
                                                                    'method' => 'post',
                                                                ],
                                                            ]) ?>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </dl>
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>