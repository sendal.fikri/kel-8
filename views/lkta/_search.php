<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LktaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lkta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'nim') ?>

    <?= $form->field($model, 'idstudi') ?>

    <?= $form->field($model, 'masastudi') ?>

    <?php // echo $form->field($model, 'idpembimbing') ?>

    <?php // echo $form->field($model, 'judulta') ?>

    <?php // echo $form->field($model, 'tglpengta') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'tglsp') ?>

    <?php // echo $form->field($model, 'semsp') ?>

    <?php // echo $form->field($model, 'statussp') ?>

    <?php // echo $form->field($model, 'waktupengsp') ?>

    <?php // echo $form->field($model, 'idruangansp') ?>

    <?php // echo $form->field($model, 'idpengujisp') ?>

    <?php // echo $form->field($model, 'nilaipemsp') ?>

    <?php // echo $form->field($model, 'nilaipengsp') ?>

    <?php // echo $form->field($model, 'rataratasp') ?>

    <?php // echo $form->field($model, 'semsh') ?>

    <?php // echo $form->field($model, 'tglmulaish') ?>

    <?php // echo $form->field($model, 'tglakhirsh') ?>

    <?php // echo $form->field($model, 'tglpengsh') ?>

    <?php // echo $form->field($model, 'persyaratansh') ?>

    <?php // echo $form->field($model, 'statussh') ?>

    <?php // echo $form->field($model, 'idruangansh') ?>

    <?php // echo $form->field($model, 'idpengujish') ?>

    <?php // echo $form->field($model, 'nilaipemsh') ?>

    <?php // echo $form->field($model, 'nilaipengsh') ?>

    <?php // echo $form->field($model, 'rataratash') ?>

    <?php // echo $form->field($model, 'tglsidangsta') ?>

    <?php // echo $form->field($model, 'persidangsta') ?>

    <?php // echo $form->field($model, 'persyaratanuangsta') ?>

    <?php // echo $form->field($model, 'statussta') ?>

    <?php // echo $form->field($model, 'waktusta') ?>

    <?php // echo $form->field($model, 'idruangansta') ?>

    <?php // echo $form->field($model, 'idpengujusta') ?>

    <?php // echo $form->field($model, 'idpengujustaa') ?>

    <?php // echo $form->field($model, 'nilaipemsta') ?>

    <?php // echo $form->field($model, 'nilaipengsta') ?>

    <?php // echo $form->field($model, 'nilaipengstaa') ?>

    <?php // echo $form->field($model, 'ratasta') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'abjad') ?>

    <?php // echo $form->field($model, 'ipk') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
