<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\widgets\DateTimePicker;
use app\models\Programstudi;
use app\models\Pembimbing;
use app\models\Ruangan;
use app\models\Tahun;
use app\models\Status;
use app\models\Penguji;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Lkta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lkta-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content" style="padding-left:0; padding-right:0">
                <div class="box box-info">
                    <section class="content">
                        <div class="box-body" style="margin-top: -10px;">
                            <legend><i class="fa fa-book"></i> &nbsp; Lembar Kendali Tugas Akhir</legend>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $form->field($model, 'idstudi')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(Programstudi::find()->all(), 'id', 'nama'),
                                            'options' => ['placeholder' => 'Pilih Program Studi'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                    </div>
                                    <?= $form->field($model, 'masastudi')->dropDownList(
                                        ArrayHelper::map(Tahun::find()->all(), 'id', 'tahun'),
                                        [
                                            'prompt' => 'Pilih Tahun Angkatan',
                                            'onchange' => '
                                            $.post("index.php?r=branches/lists&id=' . '"+$(this).val(),function(data)
                                            { $("select#departments-branches_branch_id" ).html(data);
                                        });'
                                        ]
                                    ); ?>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php echo $form->field($model, 'idpembimbing')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(Pembimbing::find()->all(), 'id', 'nama'),
                                            'options' => ['placeholder' => 'Pilih Pembimbing'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($model, 'judulta')->textInput() ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($model, 'tglpengta')->widget(DatePicker::classname(), [
                                            'options' => ['placeholder' => 'Tanggal Pengajuan Tugas Akhir'],
                                            'pluginOptions' => [
                                                'autoclose' => true
                                            ]
                                        ]); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $form->field($model, 'status')->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(Status::find()->all(), 'id', 'nama'),
                                            'options' => ['placeholder' => 'Pilih Ruangan'],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="box-body">
                            <div class="detail-invoice">
                                <legend><i class="fa fa-book"></i> &nbsp;Seminar Proposal</legend>
                                <!-- <div id="w0" class="grid-view table-responsive"> -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= $form->field($model, 'tglsp')->widget(DatePicker::classname(), [
                                                'options' => ['placeholder' => 'Tanggal Pengajuan Seminar Proposal'],
                                                'pluginOptions' => [
                                                    'autoclose' => true
                                                ]
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'semsp')->textInput() ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->field($model, 'statussp')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Status::find()->all(), 'id', 'nama'),
                                                'options' => ['placeholder' => 'Pilih Ruangan'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'waktupengsp')->widget(DateTimePicker::classname(), [
                                                'name' => 'waktupengsp',
                                                'options' => ['placeholder' => '08-Apr-2020 10:20 AM'],
                                                'value' => '08-Apr-2004 10:20 AM',
                                                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                                'pluginOptions' => [
                                                    'format' => 'dd-MM-yyyy HH:ii P',
                                                    'autoclose' => true,
                                                ]
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->field($model, 'idruangansp')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Ruangan::find()->all(), 'id', 'nama'),
                                                'options' => ['placeholder' => 'Pilih Ruangan'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo $form->field($model, 'idpengujisp')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Penguji::find()->all(), 'id', 'nama'),
                                                'options' => ['placeholder' => 'Pilih Penguji'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'nilaipemsp')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'nilaipengsp')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'rataratasp')->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="history-pembayaran">
                                <legend><i class="fa fa-book"></i> &nbsp;Seminar Hasil</legend>
                                <!-- <div id="w1" class="grid-view table-responsive"> -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= $form->field($model, 'semsh')->textInput() ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'tglmulaish')->widget(DatePicker::classname(), [
                                                'options' => ['placeholder' => 'Tanggal Mulai Seminar Hasil'],
                                                'pluginOptions' => [
                                                    'autoclose' => true
                                                ]
                                            ]); ?>
                                        </div>
                                        <div class=" form-group">
                                            <?= $form->field($model, 'tglakhirsh')->widget(DatePicker::classname(), [
                                                'options' => ['placeholder' => 'Tanggal Akhir Seminar Hasil'],
                                                'pluginOptions' => [
                                                    'autoclose' => true
                                                ]
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'tglpengsh')->widget(DatePicker::classname(), [
                                                'options' => ['placeholder' => 'Tanggal Pengajuan Seminar Hasil'],
                                                'pluginOptions' => [
                                                    'autoclose' => true
                                                ]
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'persyaratansh')->textInput() ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->field($model, 'statussh')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Status::find()->all(), 'id', 'nama'),
                                                'options' => ['placeholder' => 'Pilih Status'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo $form->field($model, 'idruangansh')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Ruangan::find()->all(), 'id', 'nama'),
                                                'options' => ['placeholder' => 'Pilih Ruangan'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->field($model, 'idpengujish')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Penguji::find()->all(), 'id', 'nama'),
                                                'options' => ['placeholder' => 'Pilih Penguji'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'nilaipemsh')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'nilaipengsh')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'rataratash')->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="history-pembayaran">
                                <legend><i class="fa fa-book"></i> &nbsp;Sidang Tugas Akhir</legend>
                                <!-- <div id="w1" class="grid-view table-responsive"> -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= $form->field($model, 'tglsidangsta')->widget(DatePicker::classname(), [
                                                'options' => ['placeholder' => 'Tanggal Sidang Tugas Akhir'],
                                                'pluginOptions' => [
                                                    'autoclose' => true
                                                ]
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'persidangsta')->textInput() ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'persyaratanuangsta')->textInput() ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->field($model, 'statussta')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Status::find()->all(), 'id', 'nama'),
                                                'options' => ['placeholder' => 'Pilih Status'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'waktusta')->widget(DateTimePicker::classname(), [
                                                'name' => 'waktusta',
                                                'options' => ['placeholder' => '08-Apr-2020 10:20 AM'],
                                                'value' => '08-Apr-2004 10:20 AM',
                                                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                                'pluginOptions' => [
                                                    'format' => 'dd-MM-yyyy HH:ii P',
                                                    'autoclose' => true,
                                                ]
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->field($model, 'idruangansta')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Ruangan::find()->all(), 'id', 'nama'),
                                                'options' => ['placeholder' => 'Pilih Ruangan'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?php echo $form->field($model, 'idpengujusta')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Penguji::find()->all(), 'id', 'nama'),
                                                'options' => ['placeholder' => 'Pilih Penguji 1'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?php echo $form->field($model, 'idpengujustaa')->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Penguji::find()->all(), 'id', 'nama'),
                                                'options' => ['placeholder' => 'Pilih Penguji 2'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]); ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'nilaipemsta')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'nilaipengsta')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'nilaipengstaa')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'ratasta')->textInput(['maxlength' => true]) ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="history-pembayaran">
                                <div class="row" style="margin-top: -25px;"><br><br>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= $form->field($model, 'total')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class=" form-group">
                                            <?= $form->field($model, 'abjad')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'ipk')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <td>
                                            <?= Html::submitButton('Simpan', ['class' => 'btn btn-block btn-success']) ?>
                                        </td> <br>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <!-- </div> -->
                            </div>
                        </div>
                    </section>
                </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>