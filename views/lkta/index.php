<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LktaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LKTA';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lkta-index">
    <div class="content-wrapper">
        <div class="container">
            <!-- Main content -->
            <section class="content" style="padding-left:0; padding-center:0">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <div class="col-md-10">
                            <h4><i class="fa fa-book"></i> Lembar Kendali Tugas Akhir</h4>
                        </div>
                        <div class="col-md-2" style="text-align: right">
                            <p>
                                <?= Html::a('Input Data', ['create'], ['class' => 'btn btn-success']) ?>
                            </p>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">

                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    // 'id',
                                    'nama',
                                    // 'nim',
                                    // 'idstudi',
                                    // 'masastudi',
                                    'judulta:ntext',
                                    'idpembimbing',
                                    //'tglpengta',
                                    //'status',
                                    //'tglsp',
                                    //'semsp:ntext',
                                    //'statussp',
                                    //'waktupengsp',
                                    //'idruangansp',
                                    //'idpengujisp',
                                    //'nilaipemsp',
                                    //'nilaipengsp',
                                    'rataratasp',
                                    //'semsh:ntext',
                                    //'tglmulaish',
                                    //'tglakhirsh',
                                    //'tglpengsh',
                                    //'persyaratansh:ntext',
                                    //'statussh',
                                    //'idruangansh',
                                    //'idpengujish',
                                    //'nilaipemsh',
                                    //'nilaipengsh',
                                    'rataratash',
                                    //'tglsidangsta',
                                    //'persidangsta:ntext',
                                    //'persyaratanuangsta:ntext',
                                    //'statussta',
                                    //'waktusta',
                                    //'idruangansta',
                                    //'idpengujusta',
                                    //'idpengujustaa',
                                    //'nilaipemsta',
                                    //'nilaipengsta',
                                    //'nilaipengstaa',
                                    'ratasta',
                                    //'total',
                                    //'abjad',
                                    // 'ipk',

                                    ['class' => 'yii\grid\ActionColumn'],
                                ],
                            ]); ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
        </div>
        <!-- /.content -->
    </div>
</div>